"use strict"
// 1) DOM - це модель документа, яку браузер створює в пам'яті комп'ютера на підставі коду HTML, отриманого ним від сервера.


// 2) innerText  встановлює вміст тега  простим текстом. Показує весь текстовий вміст, який не відноситься до синтаксису HTML.
// innerHTML встановлює вміст у форматі HTML. Показує текстову інформацію по одному елементу.


// 3) Якщо елемент має атрибут id - метод document.getElementById(id).
// Найуніверсальніший метод - elem.querySelectorAll(css) => повертає всі елементи всередині elem, що відповідають заданому CSS-селектору.





// Task 1
let paragraphs = document.querySelectorAll("p");

paragraphs.forEach(el => {
   el.style.backgroundColor="#ff0000";
});

// // Task 2

let options = document.getElementById('optionsList');
console.log(options);
console.log(options.parentElement);
console.log(options.childNodes);
[...options.childNodes].forEach(el => console.log(el.nodeName)
);
[...options.childNodes].forEach(elem => console.log(elem.nodeType)
);

// // Task 3, 4

let testP = document.getElementById('testParagraph').innerHTML = "This is a paragraph";
console.log(testP);

// Task 5
let mainHeader =  document.querySelector(".main-header");


[...mainHeader.children].forEach(el => el.classList.add('nav-item')
);
console.log(mainHeader.children);

// Task 6

let task6 = document.querySelectorAll('.section-title');
console.log(task6);

for (const el of task6) {
   el.classList.remove('section-title');
}
console.log(task6);



